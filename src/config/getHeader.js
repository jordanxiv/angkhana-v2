export const headerAuth = {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: localStorage.getItem('token')
}

export const headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
}

export const withToken = (cb) => {
    cb({
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem('token')
    })
}