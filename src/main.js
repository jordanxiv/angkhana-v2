import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store/index'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
require('@/assets/css/default.scss')
require('@/assets/css/font-awesome/css/all.css')

Vue.use(BootstrapVue);
Vue.config.productionTip = false;

axios.defaults.baseURL = 'http://localhost:3000/api';
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
